---
wsId: easyMarkets
title: easyMarkets Online Trading
altTitle: 
authors:
- danny
appId: com.easyforex.trading
appCountry: us
idd: 348823316
released: 2010-01-05
updated: 2022-04-18
version: '4.30'
stars: 4.6
reviews: 249
size: '153463808'
website: https://www.easymarkets.com/int/platforms/easymarkets-mobile-app/
repository: 
issue: 
icon: com.easyforex.trading.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-01
signer: 
reviewArchive: 
twitter: easymarkets
social:
- https://www.linkedin.com/company/easymarkets
- https://www.facebook.com/easyMarkets

---

{% include copyFromAndroid.html %}

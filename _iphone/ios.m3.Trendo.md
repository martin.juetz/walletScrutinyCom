---
wsId: trendofx
title: Forex Trading - Trendo
altTitle: 
authors:
- danny
appId: ios.m3.Trendo
appCountry: in
idd: 1530580389
released: 2020-09-29
updated: 2022-05-16
version: 2.8.81
stars: 0
reviews: 0
size: '87764992'
website: 
repository: 
issue: 
icon: ios.m3.Trendo.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}

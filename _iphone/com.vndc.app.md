---
wsId: VNDCPro
title: 'ONUS: Buy BTC, ETH, DOGE'
altTitle: 
authors:
- danny
appId: com.vndc.app
appCountry: us
idd: 1498452975
released: 2020-03-09
updated: 2022-05-25
version: 2.3.8
stars: 4.7
reviews: 816
size: '198438912'
website: https://goonus.io/en
repository: 
issue: 
icon: com.vndc.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: vncd_official
social: 

---

{% include copyFromAndroid.html %}

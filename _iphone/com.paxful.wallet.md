---
wsId: Paxful
title: Paxful | Marketplace & Wallet
altTitle: 
authors:
- leo
appId: com.paxful.wallet
appCountry: 
idd: 1443813253
released: 2019-05-09
updated: 2022-05-11
version: 2.7.10
stars: 3.6
reviews: 2423
size: '61357056'
website: https://paxful.com
repository: 
issue: 
icon: com.paxful.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: paxful
social:
- https://www.facebook.com/paxful
- https://www.reddit.com/r/paxful

---

{% include copyFromAndroid.html %}

---
wsId: AirGapVault
title: AirGap Vault - Secure Secrets
altTitle: 
authors:
- leo
appId: it.airgap.vault
appCountry: 
idd: 1417126841
released: 2018-08-24
updated: 2022-05-12
version: 3.17.2
stars: 4.2
reviews: 10
size: '92327936'
website: 
repository: https://github.com/airgap-it/airgap-vault
issue: 
icon: it.airgap.vault.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2021-09-29
signer: 
reviewArchive: 
twitter: AirGap_it
social:
- https://www.reddit.com/r/AirGap

---

This app for Android is reproducible but unfortunately due to limitations of the
iPhone platform, we so far were not able to reproduce any App Store app.

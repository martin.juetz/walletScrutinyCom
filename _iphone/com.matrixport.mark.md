---
wsId: matrixport
title: 'Matrixport: Buy & Earn Crypto'
altTitle: 
authors:
- danny
appId: com.matrixport.mark
appCountry: hk
idd: 1488557973
released: 2019-11-25
updated: 2022-05-29
version: 3.1.80
stars: 4.6
reviews: 32
size: '205005824'
website: https://invest.matrixport.dev/en
repository: 
issue: 
icon: com.matrixport.mark.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: realMatrixport
social:
- https://www.linkedin.com/company/matrixport
- https://www.facebook.com/matrixport
- https://www.reddit.com/r/Matrixport

---

{% include copyFromAndroid.html %}

---
wsId: LBank
title: LBank - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.LBank.LBankNavApp
appCountry: us
idd: 1437346368
released: 2019-02-22
updated: 2022-05-16
version: 4.9.33
stars: 4
reviews: 419
size: '230905856'
website: https://www.lbank.info/
repository: 
issue: 
icon: com.LBank.LBankNavApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: LBank_Exchange
social:
- https://www.linkedin.com/company/lbank
- https://www.facebook.com/LBank.info

---

{% include copyFromAndroid.html %}

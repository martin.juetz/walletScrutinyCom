---
wsId: ZamZam
title: Zamzam - money transfers
altTitle: 
authors:
- danny
appId: com.zamzam.bank
appCountry: ru
idd: 1521900439
released: 2020-07-04
updated: 2022-05-17
version: 1.4.5
stars: 3.5
reviews: 35
size: '70700032'
website: https://zam.me
repository: 
issue: 
icon: com.zamzam.bank.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-30
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
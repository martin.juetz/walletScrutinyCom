---
wsId: brasilBitcoin
title: Brasil Bitcoin Exchange
altTitle: 
authors:
- danny
appId: br.com.brasilbitcoin.run
appCountry: br
idd: 1519300849
released: 2020-07-27
updated: 2022-05-04
version: 2.7.07
stars: 4.9
reviews: 211
size: '77534208'
website: 
repository: 
issue: 
icon: br.com.brasilbitcoin.run.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: brbtcoficial
social:
- https://www.facebook.com/brbtcoficial

---

{% include copyFromAndroid.html %}

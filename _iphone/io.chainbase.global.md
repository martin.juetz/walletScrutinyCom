---
wsId: hotbit
title: Hotbit-Global
altTitle: 
authors:
- danny
appId: io.chainbase.global
appCountry: 
idd: 1568969341
released: 2021-05-26
updated: 2022-05-25
version: 1.4.7
stars: 3.5
reviews: 682
size: '49627136'
website: https://www.hotbit.io
repository: 
issue: 
icon: io.chainbase.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Hotbit_news
social:
- https://www.linkedin.com/company/hotbitexchange
- https://www.facebook.com/hotbitexchange

---

{% include copyFromAndroid.html %}

---
wsId: wazirx
title: 'WazirX: Buy Bitcoin & Crypto'
altTitle: 
authors: 
appId: com.wrx.wazirx
appCountry: in
idd: 1349082789
released: 2018-03-07
updated: 2022-05-10
version: 1.17.1
stars: 4.3
reviews: 45946
size: '36666368'
website: https://wazirx.com
repository: 
issue: 
icon: com.wrx.wazirx.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive: 
twitter: WazirxIndia
social:
- https://www.linkedin.com/company/wazirx
- https://www.facebook.com/wazirx

---

As this exchange allows holding your BTC in the app such
as sending and receiving them, it is usable as a wallet. A custodial wallet. As
such it is **not verifiable**.

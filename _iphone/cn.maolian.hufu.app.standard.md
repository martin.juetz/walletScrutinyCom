---
wsId: hoo
title: Hoo
altTitle: 
authors:
- danny
appId: cn.maolian.hufu.app.standard
appCountry: us
idd: 1387872759
released: 2018-06-28
updated: 2022-05-20
version: 4.6.83
stars: 3.4
reviews: 105
size: '158381056'
website: https://hoo.com
repository: 
issue: 
icon: cn.maolian.hufu.app.standard.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive: 
twitter: Hoo_exchange
social:
- https://www.facebook.com/hooexchange
- https://www.reddit.com/r/HooExchange

---

{% include copyFromAndroid.html %}

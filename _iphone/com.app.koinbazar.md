---
wsId: koinbazar
title: Koinbazar
altTitle: 
authors:
- danny
appId: com.app.koinbazar
appCountry: in
idd: 1567360326
released: 2021-06-02
updated: 2022-05-24
version: '1.20'
stars: 2.6
reviews: 48
size: '58445824'
website: https://www.koinbazar.com/
repository: 
issue: 
icon: com.app.koinbazar.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: koinbazar
social:
- https://www.linkedin.com/company/koinbazar
- https://www.facebook.com/koinbazar

---

{% include copyFromAndroid.html %}

---
wsId: roseon
title: Roseon WonderApp
altTitle: 
authors:
- danny
appId: com.roseon.finance.production
appCountry: vn
idd: 1559440997
released: 2021-05-24
updated: 2022-05-04
version: 2.3.30
stars: 4.7
reviews: 37
size: '132276224'
website: https://roseon.finance/
repository: 
issue: 
icon: com.roseon.finance.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: RoseonFinance
social:
- https://www.facebook.com/Roseon.Finance

---

{% include copyFromAndroid.html %}

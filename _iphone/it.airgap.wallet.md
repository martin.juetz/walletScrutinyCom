---
wsId: AirGapWallet
title: AirGap Wallet
altTitle: 
authors:
- danny
appId: it.airgap.wallet
appCountry: 
idd: 1420996542
released: 2018-08-24
updated: 2022-05-10
version: 3.18.1
stars: 3.9
reviews: 17
size: '108036096'
website: https://airgap.it/
repository: 
issue: 
icon: it.airgap.wallet.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-03-07
signer: 
reviewArchive: 
twitter: AirGap_it
social:
- https://www.reddit.com/r/AirGap

---

{% include copyFromAndroid.html %}

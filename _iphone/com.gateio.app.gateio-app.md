---
wsId: gate.io
title: Gate.io-Buy BTC,ETH,SHIB
altTitle: 
authors:
- danny
appId: com.gateio.app.gateio-app
appCountry: id
idd: 1294998195
released: 2017-11-03
updated: 2022-05-19
version: 3.5.1
stars: 2.9
reviews: 111
size: '426037248'
website: https://gate.io
repository: 
issue: 
icon: com.gateio.app.gateio-app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: gate_io
social:
- https://www.facebook.com/gateioglobal
- https://www.reddit.com/r/GateioExchange

---

{% include copyFromAndroid.html %}

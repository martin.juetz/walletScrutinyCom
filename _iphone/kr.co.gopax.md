---
wsId: gopax
title: GOPAX
altTitle: 
authors:
- danny
appId: kr.co.gopax
appCountry: kr
idd: 1369896843
released: 2018-06-21
updated: 2022-05-30
version: 1.9.1
stars: 2.8
reviews: 444
size: '109862912'
website: https://www.gopax.co.kr/notice
repository: 
issue: 
icon: kr.co.gopax.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

 {% include copyFromAndroid.html %}

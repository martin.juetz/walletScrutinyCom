---
wsId: maiarwallet
title: Maiar
altTitle: 
authors:
- danny
appId: com.elrond.maiar.wallet
appCountry: us
idd: 1519405832
released: 2021-01-31
updated: 2022-05-25
version: 1.4.22
stars: 4.7
reviews: 339
size: '303873024'
website: https://maiar.com/
repository: 
issue: 
icon: com.elrond.maiar.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-05
signer: 
reviewArchive: 
twitter: getMaiar
social:
- https://www.linkedin.com/company/getmaiar
- https://www.facebook.com/getMaiar

---

{% include copyFromAndroid.html %}

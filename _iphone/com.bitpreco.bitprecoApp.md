---
wsId: BitPreco
title: BitPreço Oficial
altTitle: 
authors:
- danny
appId: com.bitpreco.bitprecoApp
appCountry: br
idd: 1545825554
released: 2021-03-18
updated: 2022-05-20
version: 1.8.25
stars: 4.7
reviews: 397
size: '74125312'
website: https://bitpreco.com/
repository: 
issue: 
icon: com.bitpreco.bitprecoApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-16
signer: 
reviewArchive: 
twitter: BitPreco
social:
- https://www.linkedin.com/company/bitpreco
- https://www.facebook.com/BitPreco

---

{% include copyFromAndroid.html %}


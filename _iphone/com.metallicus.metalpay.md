---
wsId: MetalPay
title: Metal Pay
altTitle: 
authors:
- danny
appId: com.metallicus.metalpay
appCountry: us
idd: 1345101178
released: 2018-09-14
updated: 2022-05-11
version: 2.9.0
stars: 4.3
reviews: 4139
size: '129632256'
website: https://metalpay.com
repository: 
issue: 
icon: com.metallicus.metalpay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: metalpaysme
social:
- https://www.facebook.com/metalpaysme
- https://www.reddit.com/r/MetalPay

---

{% include copyFromAndroid.html %}


---
wsId: 
title: 'Ledger Live: Crypto & NFT App'
altTitle: 
authors: 
users: 500000
appId: com.ledger.live
appCountry: 
released: 2019-01-27
updated: 2022-05-24
version: 3.1.2
stars: 3
ratings: 6090
reviews: 905
size: 
website: https://www.ledger.com
repository: 
issue: 
icon: com.ledger.live.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2020-11-17
signer: 
reviewArchive: 
twitter: Ledger
social:
- https://www.linkedin.com/company/ledgerhq
- https://www.facebook.com/Ledger
redirect_from:
- /com.ledger.live/

---

This is the companion app for the Ledger hardware wallets. As we fail to start
the app without connecting a hardware wallet, it is very likely this app is not
designed to store private keys or in other words it is probably impossible to
spend with this app without confirming each and every transaction on the
hardware wallet itself and is thus **not a wallet** itself.
